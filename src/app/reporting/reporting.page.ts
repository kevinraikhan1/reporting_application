import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-reporting',
    templateUrl: './reporting.page.html',
    styleUrls: ['./reporting.page.scss'],
})
export class ReportingPage implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

    btnClicked() {

        alert('btn clicked');
    }

    report(test) {
        let tulisan = document.getElementById('textReport').value;
        if (tulisan.length == 0) {
            alert('Tidak boleh kosong');
        } else {
            alert(tulisan + "\nTerimakasih sudah mengirimkan report");
        }


    }

}
